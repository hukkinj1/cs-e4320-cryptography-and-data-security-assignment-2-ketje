#ifndef KETJE_H
#define KETJE_H

/* Perform the Ketje Major authenticated encryption operation on a message.
 *
 * cryptogram - the output buffer for the ciphertext, allocated by the caller.
 *              The buffer is the same size as the "data" plaintext buffer.
 * tag        - the output buffer for the tag, allocated by the caller.
 * t_len      - the requested tag length in bits.
 * key        - the key, provided by the caller.
 * k_len      - the key length in bits.
 * nonce      - the nonce, provided by the caller.
 * n_len      - the nonce length in bits.
 * data       - the plaintext, provided by the caller.
 * d_len      - the plaintext length in bits.
 * header     - the additional plaintext, provided by the caller.
 * h_len      - the additional plaintext length in bits.
 */
void ketje_mj_e(unsigned char *cryptogram,
		unsigned char *tag, unsigned int t_len,
		const unsigned char *key, unsigned int k_len,
		const unsigned char *nonce, unsigned int n_len,
		const unsigned char *data, unsigned long d_len,
		const unsigned char *header, unsigned long h_len);

/* You can add your own functions below this line.
 * Do NOT modify anything above. */

typedef struct _MonkeyParams MonkeyParams;

unsigned char *monkeyduplex_start(unsigned char *input, int input_len, MonkeyParams monkey_params);
void monkeyduplex_action(unsigned char *output, int output_len, unsigned char *input, int input_len, unsigned char *state, MonkeyParams monkey_params, int rounds);
void monkeyduplex_step(unsigned char *output, int output_len, unsigned char *input, int input_len, unsigned char *state, MonkeyParams monkey_params);
void monkeyduplex_stride(unsigned char *output, int output_len, unsigned char *input, int input_len, unsigned char *state, MonkeyParams monkey_params);

unsigned char *monkeywrap_init(const unsigned char *key, unsigned int key_len,
							   const unsigned char *nonce, unsigned int nonce_len,
							   MonkeyParams monkey_params);

void monkeywrap_wrap(unsigned char *output_cryptogram,
					 unsigned char *output_tag, unsigned int tag_len,
					 const unsigned char *header, unsigned int header_len,
					 const unsigned char *body, unsigned int body_len,
					 unsigned char *state, MonkeyParams monkey_params);

void pack_key(unsigned char* output, const unsigned char *key, int key_len, int keypack_len);
int block_len(int bit_len, int block_size);
unsigned int extract_block(unsigned char *output, const unsigned char *input, int input_len, int block_size, int block_index);
unsigned int get_block_size(unsigned int input_len, unsigned int block_size, unsigned int block_index);
void xor_n_bytes(unsigned char *output, const unsigned char *arr1, const unsigned char *arr2, unsigned int n);

#endif				/* KETJE_H */
