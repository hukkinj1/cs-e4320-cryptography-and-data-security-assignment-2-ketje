#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "keccak.h"

/* Constants */
static const int L = 6;
static const int LANE_LENGTH = 64; // == pow(2, L)
static const int STATE_ARRAY_SIZE = 1600; // == LANE_LENGTH * 25

/* Useful macros */
/* Rotate a 64b word to the left by n positions */
#define ROL64(a, n) ((((n)%64) != 0) ? ((((uint64_t)a) << ((n)%64)) ^ (((uint64_t)a) >> (64-((n)%64)))) : a)

/* Function prototypes */
unsigned char rc(unsigned int t);

/* Perform the KECCAK-p*[b, n_r] algorithm
 *
 * S  - the input bit string
 * b  - the length of the input string in bits
 * nr - the number of rounds
 * l  - the value of l associated with b (log2(b/25))
 *
 * Returns a pointer to the output bit string
 */
unsigned char *keccak_p_star(unsigned char *S, unsigned long b, int nr, int l)
{
	/* Implement this function using the code you wrote for Assignment 1.
	 * You will need to implement one extra function, the permutation
	 * pi^(-1) (the inverse of pi) described in Section 2.1 of the Ketje
	 * document and Section 8 of the Assignment 2 instructions.
	 */

	// Just a hack for mentioning l to get rid of '-Wunused parameter'. This does absolutely nothing.
	if (l == 0) {
		;
	}

	/* Currently only b == 1600 is supported. */
	if (b != 1600) {
		return NULL;
	}

	unsigned char *output = NULL;
	output = malloc(byte_len(b));
	if (output == NULL) {
		return NULL;
	}

	/* Create two temporary state arrays that are given as input and output
	 * to the functions */
	unsigned char temp1[byte_len(b)];
	unsigned char temp2[byte_len(b)];

	step_mapping_3_pi_inverse(temp1, S);
	keccak_p_1600(temp2, temp1, nr);
	step_mapping_3_pi(output, temp2);
	return output;
}

/* Copy n bits from a buffer to another.
 *
 * dst   - the destination buffer, allocated by the caller
 * dst_o - the bit offset in the destination buffer
 * src   - the source buffer, allocated by the caller
 * src_o - the bit offset in the source buffer
 * n     - the number of bits to copy
 *
 * n does not need to be a multiple of 8.
 * dst and src must be at least ceiling(n/8) bytes long.
 */
void cpynbits(unsigned char *dst, unsigned int dst_o,
	      const unsigned char *src, unsigned int src_o, unsigned int n)
{
	unsigned int v;
	unsigned int s_bit_cursor, s_byte_cursor, d_bit_cursor, d_byte_cursor;
	// Initialise cursors
	s_byte_cursor = src_o / 8;
	s_bit_cursor = src_o % 8;
	d_byte_cursor = dst_o / 8;
	d_bit_cursor = dst_o % 8;

	// If both cursors are byte-aligned, and n is a multiple of 8 bits
	if (s_bit_cursor == 0 && d_bit_cursor == 0 && n % 8 == 0) {
		// Just copy n/8 bytes byte by byte from src to dst
		for (unsigned int i = 0; i < n / 8; i++) {
			dst[d_byte_cursor + i] = src[s_byte_cursor + i];
		}
	} else {
		// Copy n bits bit by bit from src to dst
		for (unsigned long i = 0; i < n; i++) {
			// Get the bit
			v = ((src[s_byte_cursor] >> s_bit_cursor) & 1);
			// Set the bit
			dst[d_byte_cursor] ^=
			    (-v ^ dst[d_byte_cursor]) & (1 << d_bit_cursor);
			// Increment cursors
			if (++s_bit_cursor == 8) {
				s_byte_cursor++;
				s_bit_cursor = 0;
			}
			if (++d_bit_cursor == 8) {
				d_byte_cursor++;
				d_bit_cursor = 0;
			}
		}
	}
}

/* Concatenate two bit strings (X||Y)
 *
 * Z     - the output bit string. The array is allocated by this function: the
 *         caller must take care of freeing it after use.
 * X     - the first bit string
 * X_len - the length of the first string in bits
 * Y     - the second bit string
 * Y_len - the length of the second string in bits
 *
 * Returns the length of the output string in bits. The length in Bytes of the
 * output C array is ceiling(output_bit_len/8).
 */
unsigned long concatenate(unsigned char **Z, const unsigned char *X,
			  unsigned long X_len, const unsigned char *Y,
			  unsigned long Y_len)
{
	/* The bit length of Z: the sum of X_len and Y_len */
	unsigned long Z_bit_len = X_len + Y_len;
	/* The byte length of Z:
	 * the least multiple of 8 greater than X_len + Y_len */
	unsigned long Z_byte_len = (Z_bit_len / 8) + (Z_bit_len % 8 ? 1 : 0);
	// Allocate the output string and initialize it to 0
	*Z = calloc(Z_byte_len, sizeof(unsigned char));
	if (*Z == NULL)
		return 0;
	// Copy X_len bits from X to Z
	cpynbits(*Z, 0, X, 0, X_len);
	// Copy Y_len bits from Y to Z
	cpynbits(*Z, X_len, Y, 0, Y_len);

	return Z_bit_len;
}

/* Concatenate the 00, 01, 10, or 11 bit string to a given bit string
 * e.g. (X||00), (X||01), (X||10), (X||11)
 * Due to the KECCAK bit string representation, the bit strings are represented
 * as bytes respectively as:
 *       00 -> 0x00
 *       01 -> 0x02
 *       10 -> 0x01
 *       11 -> 0x03
 *
 * Z     - the output bit string. The array is allocated by this function: the
 *         caller must take care of freeing it after use.
 * X     - the bit string
 * X_len - the length of the string in bits
 *
 * Returns the length of the output string in bits. The length in Bytes of the
 * output C array is ceiling(output_bit_len/8).
 */
unsigned long concatenate_00(unsigned char **Z, const unsigned char *X,
			     unsigned long X_len)
{
	unsigned char zeroes[] = { 0x00 };
	return concatenate(Z, X, X_len, zeroes, 2);
}

unsigned long concatenate_01(unsigned char **Z, const unsigned char *X,
			     unsigned long X_len)
{
	unsigned char zeroone[] = { 0x02 };
	return concatenate(Z, X, X_len, zeroone, 2);
}

unsigned long concatenate_10(unsigned char **Z, const unsigned char *X,
			     unsigned long X_len)
{
	unsigned char onezero[] = { 0x01 };
	return concatenate(Z, X, X_len, onezero, 2);
}

unsigned long concatenate_11(unsigned char **Z, const unsigned char *X,
			     unsigned long X_len)
{
	unsigned char ones[] = { 0x03 };
	return concatenate(Z, X, X_len, ones, 2);
}

/* Performs the pad10*1(x, m) algorithm
 *
 * P - the output bit string. The array is allocated by this function: the
 *     caller must take care of freeing it after use.
 * x - the alignment value
 * m - the existing string length in bits
 *
 * Returns the length in bits of the output bit string.
 */
unsigned long pad10x1(unsigned char **P, unsigned int x, unsigned int m)
{
	/* 1. j = (-m-2) mod x */
	long j = (2 * x - 2 - (m % x)) % x;
	/* 2. P = 1 || zeroes(j) || 1 */
	// Compute P bit and byte length
	unsigned long P_bit_len = 2 + j;
	unsigned long P_byte_len = (P_bit_len / 8) + (P_bit_len % 8 ? 1 : 0);
	// Allocate P and initialize to 0
	*P = calloc(P_byte_len, sizeof(unsigned char));
	if (*P == NULL)
		return 0;
	// Set the 1st bit of P to 1
	(*P)[0] |= 1;
	// Set the last bit of P to 1
	(*P)[P_byte_len - 1] |= (1 << (P_bit_len - 1) % 8);

	return P_bit_len;
}

/* Perform the rc(t) algorithm
 *
 * t - the number of rounds to perform in the LFSR
 *
 * Returns a single bit stored as the LSB of an unsigned char.
 */
unsigned char rc(unsigned int t)
{
	unsigned int tmod = t % 255;
	/* 1. If t mod255 = 0, return 1 */
	if (tmod == 0)
		return 1;
	/* 2. Let R = 10000000
	 *    The LSB is on the right: R[0] = R &0x80, R[8] = R &1 */
	unsigned char R = 0x80, R0;
	/* 3. For i from 1 to t mod 255 */
	for (unsigned int i = 1; i <= tmod; i++) {
		/* a. R = 0 || R */
		R0 = 0;
		/* b. R[0] ^= R[8] */
		R0 ^= (R & 1);
		/* c. R[4] ^= R[8] */
		R ^= (R & 0x1) << 4;
		/* d. R[5] ^= R[8] */
		R ^= (R & 0x1) << 3;
		/* e. R[6] ^= R[8] */
		R ^= (R & 0x1) << 2;
		/* Shift right by one */
		R >>= 1;
		/* Copy the value of R0 in */
		R ^= R0 << 7;
	}
	/* 4. Return R[0] */
	return R >> 7;
}

/* Performs the keccak-p algorithm with pre-set value for lane length (1600 bits).
 *
 * output - the output bit string. The caller must allocate 1600 bits for it.
 * input  - the input bit string
 * rounds - the number of step mapping rounds
 *
 */
void keccak_p_1600(unsigned char *output, const unsigned char *input, int rounds)
{
    /* Initialize output array with a copy of input bit string */
    for (int i = 0; i < (STATE_ARRAY_SIZE / 8); i++) {
        output[i] = input[i];
    }

    /* Execute total_rounds amount of step mapping rounds */
    for (int round = 12+2*L-rounds; round <= 12+2*L-1; round++) {
        Rnd(output, output, round);
    }
}

/* Performs the keccak[c] algorithm.
 *
 * c          - Capacity
 * output     - The output bit string. The caller must allocate output_len bits for it.
 * output_len - Amount of bits in the output bit string.
 * input      - The input bit string
 * input_len  - Amount of bits in the input bit string
 *
 */
void keccak(const int c, unsigned char *output, int output_len, const unsigned char *input, int input_len)
{
	const int keccak_p_rounds = 24;
    unsigned int r = LANE_LENGTH * 25 - c;

    /* Steps of Sponge algorithm:
     *
     * Step 1 */
    /* Apppend padding to input string so that it can be split into n pieces of the same length */
    unsigned char *padding;
    unsigned long padding_len = pad10x1(&padding, r, input_len);  // Allocates memory in address *padding
    unsigned char *P;
    unsigned long P_len = concatenate(&P, input, input_len, padding, padding_len);  // Allocates memory in address *P
    free(padding);

    /* Step 2 */
    int n = P_len / r;

    /* Steps 3 and 4 (nothing to do here) */

    /* Step 5 */
    /* Create bit string S that consists of b amount of zeros */
    unsigned char S[LANE_LENGTH * 25 / 8];
    memset(S, 0, LANE_LENGTH * 25 / 8);

    /* Step 6 */
    for (int i = 0; i < n; i++) {

        /* Create a bit string of b bits. Then copy P[i] over the first r bytes. End result
         * is c zeros appended to P[i] */
        unsigned char P_i_plus_zeros[LANE_LENGTH * 25 / 8];
        memset(P_i_plus_zeros, 0, LANE_LENGTH * 25 / 8);
        get_bits_from_index(P_i_plus_zeros, P, i*r, r);

        // XOR array S with array P_i_plus_zeros
        for (int j = 0; j < (LANE_LENGTH * 25 / 8); j++) {
            S[j] = S[j] ^ P_i_plus_zeros[j];
        }
        keccak_p_1600(S, S, keccak_p_rounds);
    }
    free(P);

    /* Step 7 */

    /* Maximum number of bits required for array Z is (output_len - 1 + r).
     * Lets reserve that amount in bytes, plus one extra byte, because of the rounding going down */
    unsigned char Z[((output_len-1+r) / 8) + 1];
    // Let's keep the amount of bits stored to Z here
    int Z_len = 0;

    /* Step 8, 9 and 10 */
    while (1) {
        unsigned char trunc_s[(r / 8) + 1];
        get_bits_from_index(trunc_s, S, 0, r);

        write_bits_to_index(Z, trunc_s, Z_len, r);
        Z_len = Z_len + r;
        if (output_len <= Z_len) {
            get_bits_from_index(output, Z, 0, output_len);
            return;
        }
        keccak_p_1600(S, S, keccak_p_rounds);
    }

}

/* Returns index in a state array, when given a corresponding 3D-coordinate
 * as parameter
 *
 * x    - Index of the sheet in a state array
 * y    - Index of a plane in a state array
 * z    - Index of a slice in a state array
 *
 * Returns index number of a one-dimensional state array
 */
int get_state_array_index(int x, int y, int z)
{
    return LANE_LENGTH*(5*y+x)+z;
}

/* Returns index in a "column array", when given a corresponding 2D-coordinate
 * as parameter. In a column array, one column of a state array is represented
 * by a single bit, so essentially it resembles one plane of a state array.
 *
 * x    - Index of the sheet in a state array
 * z    - Index of a slice in a state array
 *
 * Returns index number of a one-dimensional "column array"
 */
int get_column_array_index(int x, int z)
{
    return LANE_LENGTH*x+z;
}

/* Performs the first step mapping (called theta) of a round of keccak-p algorithm.
 *
 * output - the output bit string. The caller must allocate (LANE_LENGTH * 25) bits for it.
 * input - the input bit string
 *
 */
void step_mapping_1_theta(unsigned char *output, const unsigned char *input)
{
    // Declare two "column arrays"
    unsigned char C[LANE_LENGTH * 5 / 8];
    unsigned char D[LANE_LENGTH * 5 / 8];

    /* Step 1 */
    /* Form column array C by iterating through all columns of the input state array
     * and XORring all 5 bits of the column. */
    for (int x = 0; x < 5; x++) {
        for (int z = 0; z < LANE_LENGTH; z++) {
            int value =   get_bit(input, get_state_array_index(x, 0, z))
                        ^ get_bit(input, get_state_array_index(x, 1, z))
                        ^ get_bit(input, get_state_array_index(x, 2, z))
                        ^ get_bit(input, get_state_array_index(x, 3, z))
                        ^ get_bit(input, get_state_array_index(x, 4, z));
            set_bit(C, get_column_array_index(x, z), value);
        }
    }

    /* Step 2 */
    /* Form column array D by iterating through array C and XORring neighboring bits
     * of the column array */
    for (int x = 0; x < 5; x++) {
        for (int z = 0; z < LANE_LENGTH; z++) {
            int value =   get_bit(C, get_column_array_index(mod(x - 1, 5), z))
                        ^ get_bit(C, get_column_array_index(mod(x + 1, 5), mod(z - 1, LANE_LENGTH)));
            set_bit(D, get_column_array_index(x, z), value);
        }
    }

    /* Step 3 */
    /* XOR input state array with column array D. Return the result */
    for (int y = 0; y < 5; y++) {
        for (int x = 0; x < 5; x++) {
            for (int z = 0; z < LANE_LENGTH; z++) {
                int value =   get_bit(input, get_state_array_index(x, y, z))
                            ^ get_bit(D, get_column_array_index(x, z));
                set_bit(output, get_state_array_index(x, y, z), value);
            }
        }
    }
}

/* Performs the second step mapping (called rho) of a round of keccak-p algorithm.
 *
 * output - the output bit string. The caller must allocate (LANE_LENGTH * 25) bits for it.
 * input - the input bit string
 *
 */
void step_mapping_2_rho(unsigned char *output, const unsigned char *input)
{
    /* Step 1 */
    for (int z = 0; z < LANE_LENGTH; z++) {
        int value = get_bit(input, get_state_array_index(0, 0, z));
        set_bit(output, get_state_array_index(0, 0, z), value);
    }

    /* Step 2 */
    int x = 1;
    int y = 0;

    /* Step 3 */
    for (int t = 0; t <= 23; t++) {
        for (int z = 0; z < LANE_LENGTH; z++) {
            int value = get_bit(input, get_state_array_index(x, y, mod(z-(t+1)*(t+2)/2, LANE_LENGTH)));
            set_bit(output, get_state_array_index(x, y, z), value);
        }
        int previous_x = x;
        x = y;
        y = mod(2*previous_x+3*y, 5);
    }
}

/* Performs the third step mapping (called pi) of a round of keccak-p algorithm.
 *
 * output - the output bit string. The caller must allocate (LANE_LENGTH * 25) bits for it.
 * input - the input bit string
 *
 */
void step_mapping_3_pi(unsigned char *output, const unsigned char *input)
{
    for (int y = 0; y < 5; y++) {
        for (int x = 0; x < 5; x++) {
            for (int z = 0; z < LANE_LENGTH; z++) {
                int value = get_bit(input, get_state_array_index(mod(x+3*y, 5), x, z));
                set_bit(output, get_state_array_index(x, y, z), value);
            }
        }
    }
}

/* Performs the fourth step mapping (called chi) of a round of keccak-p algorithm.
 *
 * output - the output bit string. The caller must allocate (LANE_LENGTH * 25) bits for it.
 * input - the input bit string
 *
 */
void step_mapping_4_chi(unsigned char *output, const unsigned char *input)
{
    for (int y = 0; y < 5; y++) {
        for (int x = 0; x < 5; x++) {
            for (int z = 0; z < LANE_LENGTH; z++) {
                int value = get_bit(input, get_state_array_index(x, y, z))
                        ^ (
                        (get_bit(input, get_state_array_index(mod(x+1, 5), y, z)) ^ 1)
                        & get_bit(input, get_state_array_index(mod(x+2, 5), y, z))
                        );
                set_bit(output, get_state_array_index(x, y, z), value);
            }
        }
    }
}

/* Performs the fifth and last step mapping (called iota) of a round of keccak-p algorithm.
 *
 * output - the output bit string. The caller must allocate (LANE_LENGTH * 25) bits for it.
 * input - the input bit string
 * round - number of the keccak-p round
 *
 */
void step_mapping_5_iota(unsigned char *output, const unsigned char *input, int round)
{
    /* Step 1 */
    for (int y = 0; y < 5; y++) {
        for (int x = 0; x < 5; x++) {
            for (int z = 0; z < LANE_LENGTH; z++) {
                int value = get_bit(input, get_state_array_index(x, y, z));
                set_bit(output, get_state_array_index(x, y, z), value);
            }
        }
    }
    /* Step 2 */
    unsigned char RC[LANE_LENGTH / 8];
    memset(RC, 0, LANE_LENGTH / 8);

    /* Step 3 */
    for (int j = 0; j <= L; j++) {
        int value = rc(j+7*round);
        set_bit(RC, pow(2, j) - 1, value);
    }
    /* Step 4 */
    for (int z = 0; z < LANE_LENGTH; z++) {
        int value = get_bit(output, get_state_array_index(0, 0, z))
                ^ get_bit(RC, z);
        set_bit(output, get_state_array_index(0, 0, z), value);
    }
}

/* Performs inverse of the third step mapping (called pi) of a round of keccak-p algorithm.
 *
 * output - the output bit string. The caller must allocate (LANE_LENGTH * 25) bits for it.
 * input - the input bit string
 *
 */
void step_mapping_3_pi_inverse(unsigned char *output, const unsigned char *input)
{
    for (int y = 0; y < 5; y++) {
        for (int x = 0; x < 5; x++) {
            for (int z = 0; z < LANE_LENGTH; z++) {
                int value = get_bit(input, get_state_array_index(y, mod(2*x+3*y, 5), z));
                set_bit(output, get_state_array_index(x, y, z), value);
            }
        }
    }
}

/* Performs one full round of step mappings of the keccak-p algorithm.
 *
 * output - the output bit string. The caller must allocate (LANE_LENGTH * 25) bits for it.
 * input - the input bit string
 * round - number of the keccak-p round
 *
 */
void Rnd(unsigned char *output, const unsigned char *input, int round)
{
    /* Create two temporary state arrays that are given as input and output
     * to the step mapping functions */
    unsigned char temp1[LANE_LENGTH * 25 / 8];
    unsigned char temp2[LANE_LENGTH * 25 / 8];

    step_mapping_1_theta(temp1, input);
    step_mapping_2_rho(temp2, temp1);
    step_mapping_3_pi(temp1, temp2);
    step_mapping_4_chi(temp2, temp1);
    step_mapping_5_iota(output, temp2, round);
}

/* Set the value 1 to a bit in a bit array
 *
 * array - bit array
 * n - index of the bit
 *
 */
void set_bit_one(unsigned char *array, int n)
{
    int byte_index = n / 8;
    int bit_index = n % 8;

    array[byte_index] = array[byte_index] | (1 << bit_index);
}

/* Set the value 0 to a bit in a bit array
 *
 * array - bit array
 * n - index of the bit
 *
 */
void set_bit_zero(unsigned char *array, int n)
{
    int byte_index = n / 8;
    int bit_index = n % 8;

    array[byte_index] = array[byte_index] & ~(1 << bit_index);
}

/* Set value 0 or 1 to a bit in a bit array
 *
 * array - bit array
 * n     - index of the bit
 * value - the value that the bit needs to be set to (0 or 1)
 *
 */
void set_bit(unsigned char *array, int n, int value)
{
    if (value) {
        set_bit_one(array, n);
    }
    else {
        set_bit_zero(array, n);
    }
}

/* Get value of a single bit in a bit array
 *
 * array - bit array
 * n - index of the bit
 *
 * Returns the value of the bit in index n (0 or 1)
 */
int get_bit(const unsigned char *array,  int n)
{
    int byte_index = n / 8;
    int bit_index = n % 8;

    return ((array[byte_index] & (1 << bit_index)) != 0);
}

/* Gets a sequence of bits from any index/position of a bit array
 *
 * output          - Output bit array. amount_of_bits amount of bits needs to be allocated by the caller.
 * input           - Input bit array.
 * first_bit_index - Index of the first bit of the sequence to be copied to output
 * amount_of_bits  - Length of the bit sequence to be copied to output
 *
 */
void get_bits_from_index(unsigned char *output, const unsigned char *input, int first_bit_index, int amount_of_bits) {
    for (int i = 0; i < amount_of_bits; i++) {
        int value = get_bit(input, first_bit_index + i);
        set_bit(output, i, value);
    }
}

/* Writes a sequence of bits to any index/position of the output bit array
 *
 * output            - Output bit array. The caller needs to make sure that the bit array is
 *                     large enough that the bits from input can be copied (index
 *                     (first_written_bit + amount_of_bits - 1) needs to exist).
 * input             - Input bit array.
 * first_written_bit - Index of the first bit of output that will be overwritten by the bits in input
 * amount_of_bits    - Length of the bit sequence to be copied to output
 *
 */
void write_bits_to_index(unsigned char *output, const unsigned char *input, int first_written_bit, int amount_of_bits) {
    for (int i = 0; i < amount_of_bits; i++) {
        int value = get_bit(input,  i);
        set_bit(output, first_written_bit + i, value);
    }
}

/* The % -operator of C doesn't behave as modulo -function should with
 * negative numbers. This modulo function should work normally with negative
 * values.
 */
int mod(int a, int b)
{
    if(b < 0) {
        return mod(a, -b);
    }
    int return_value = a % b;
    if(return_value < 0){
        return_value += b;
    }
    return return_value;
}

/* Returns the amount of bytes required to be allocated for a bit string of length bit_len
 */
int byte_len(int bit_len)
{
	return (bit_len/8) + (bit_len % 8 ? 1:0);
}

