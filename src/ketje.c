#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include "ketje.h"
#include "keccak.h"

/* Useful macros */
// Convert a bit length in the corresponding byte length, rounding up.
#define BYTE_LEN(x) ((x/8)+(x%8?1:0))

struct _MonkeyParams {
   unsigned char *(*f)(unsigned char *, unsigned long , int, int);
   int f_width;
   int f_L;
   int block_size;
   int n_start;
   int n_step;
   int n_stride;
};

/* Perform the Ketje Major authenticated encryption operation on a message.
 *
 * cryptogram - the output buffer for the ciphertext, allocated by the caller.
 *              The buffer is the same size as the "data" plaintext buffer.
 * tag        - the output buffer for the tag, allocated by the caller.
 * t_len      - the requested tag length in bits.
 * key        - the key, provided by the caller.
 * k_len      - the key length in bits.
 * nonce      - the nonce, provided by the caller.
 * n_len      - the nonce length in bits.
 * data       - the plaintext, provided by the caller.
 * d_len      - the plaintext length in bits.
 * header     - the additional plaintext, provided by the caller.
 * h_len      - the additional plaintext length in bits.
 */
void ketje_mj_e(unsigned char *cryptogram,
		unsigned char *tag, unsigned int t_len,
		const unsigned char *key, unsigned int k_len,
		const unsigned char *nonce, unsigned int n_len,
		const unsigned char *data, unsigned long d_len,
		const unsigned char *header, unsigned long h_len)
{
	/* Ketje Major-specific parameters:
	 *   f        = KECCAK-p*[1600]
	 *   rho      = 256
	 * For all Ketje instances:
	 *   n_start  = 12
	 *   n_step   = 1
	 *   n_stride = 6
	 */

	/* Implement this function */
	MonkeyParams monkey_params = {.f = &keccak_p_star,
								  .f_width = 1600,
								  .f_L = 6,
								  .block_size = 256,
								  .n_start = 12,
								  .n_step = 1,
								  .n_stride = 6
								 };

	// allocates memory
	unsigned char *state = monkeywrap_init(key, k_len, nonce, n_len, monkey_params);
	monkeywrap_wrap(cryptogram,
				    tag, t_len,
				    header, h_len,
				    data, d_len,
				    state, monkey_params);
	free(state);
}

// Allocates memory in return value address
// TODO: testing
unsigned char *monkeyduplex_start(unsigned char *input, int input_len, MonkeyParams monkey_params)
{
	unsigned char *padding;
	unsigned long padding_len = pad10x1(&padding, monkey_params.f_width, input_len); // Allocates memory in address padding

	unsigned char temp_s[byte_len(monkey_params.f_width)];
	write_bits_to_index(temp_s, input, 0, input_len);
	write_bits_to_index(temp_s, padding, input_len, padding_len);

	free(padding);

	return monkey_params.f(temp_s, monkey_params.f_width, monkey_params.n_start, monkey_params.f_L);
}

// TODO: testing
void monkeyduplex_action(unsigned char *output, int output_len, unsigned char *input, int input_len, unsigned char *state, MonkeyParams monkey_params, int rounds)
{
	int r = monkey_params.block_size + 4;

	unsigned char *padding;
	unsigned long padding_len = pad10x1(&padding, r, input_len); // Allocates memory in address padding

	unsigned char P[byte_len(monkey_params.f_width)];
	memset(P, 0, byte_len(monkey_params.f_width));

	write_bits_to_index(P, input, 0, input_len);
	write_bits_to_index(P, padding, input_len, padding_len);
	free(padding);

	// XOR array 'state' with array 'P'
	for (int i = 0; i < byte_len(monkey_params.f_width); i++) {
		state[i] = state[i] ^ P[i];
	}

	unsigned char *new_state = monkey_params.f(state, monkey_params.f_width, rounds, monkey_params.f_L); // Allocates memory in address new_state
	write_bits_to_index(state, new_state, 0, monkey_params.f_width);
	free(new_state);
	write_bits_to_index(output, state, 0, output_len);
}

// TODO: testing
void monkeyduplex_step(unsigned char *output, int output_len, unsigned char *input, int input_len, unsigned char *state, MonkeyParams monkey_params)
{
	monkeyduplex_action(output, output_len, input, input_len, state, monkey_params, monkey_params.n_step);
}

// TODO: testing
void monkeyduplex_stride(unsigned char *output, int output_len, unsigned char *input, int input_len, unsigned char *state, MonkeyParams monkey_params)
{
	monkeyduplex_action(output, output_len, input, input_len, state, monkey_params, monkey_params.n_stride);
}

// Allocates memory in return value address
// TODO: testing
unsigned char *monkeywrap_init(const unsigned char *key, unsigned int key_len,
							   const unsigned char *nonce, unsigned int nonce_len,
							   MonkeyParams monkey_params)
{
	int keypack_len = key_len + 16;
	unsigned char keypack[byte_len(keypack_len)];
	pack_key(keypack, key, key_len, keypack_len);

	int duplex_input_len = keypack_len + nonce_len;
	unsigned char duplex_input[byte_len(duplex_input_len)];
	write_bits_to_index(duplex_input, keypack, 0, keypack_len);
	write_bits_to_index(duplex_input, nonce, keypack_len, nonce_len);

	return monkeyduplex_start(duplex_input, duplex_input_len, monkey_params);
}

// TODO: testing
void monkeywrap_wrap(unsigned char *output_cryptogram,
					 unsigned char *output_tag, unsigned int tag_len,
					 const unsigned char *header, unsigned int header_len,
					 const unsigned char *body, unsigned int body_len,
					 unsigned char *state, MonkeyParams monkey_params)
{
	int header_block_len = block_len(header_len, monkey_params.block_size);
	int body_block_len = block_len(body_len, monkey_params.block_size);

	int step_input_bytes = byte_len(monkey_params.block_size + 2);
	unsigned char step_input[step_input_bytes];

	for (int i = 0; i <= (header_block_len - 2); i++) {
		memset(step_input, 0, step_input_bytes);
		extract_block(step_input, header, header_len, monkey_params.block_size, i);
		/* A trash bin array of one byte. Nothing will be written here, because output_len
		 * of monkeyduplex_step below is 0. */
		unsigned char trash_bin[1];
		monkeyduplex_step(trash_bin, 0,
						  step_input, monkey_params.block_size + 2,
						  state, monkey_params);
	}
	unsigned char Z[byte_len(monkey_params.block_size)];
	memset(step_input, 0, step_input_bytes);
	int extracted_block_size = extract_block(step_input, header, header_len, monkey_params.block_size, header_block_len - 1);
	set_bit_one(step_input, extracted_block_size + 1);

	unsigned int body_0_size = get_block_size(body_len, monkey_params.block_size, 0);
	monkeyduplex_step(Z, body_0_size,
					  step_input, extracted_block_size + 2,
					  state, monkey_params);

	xor_n_bytes(output_cryptogram, body, Z, byte_len(body_0_size));
	for (int i = 0; i <= (body_block_len - 2); i++) {
		memset(step_input, -1, step_input_bytes);
		extracted_block_size = extract_block(step_input, body, body_len, monkey_params.block_size, i);
		unsigned int body_i_plus_one_size = get_block_size(body_len, monkey_params.block_size, i+1);
		monkeyduplex_step(Z, body_i_plus_one_size,
						  step_input, extracted_block_size + 2,
						  state, monkey_params);
		unsigned char cryptogram_i_plus_one[byte_len(body_i_plus_one_size)];
		xor_n_bytes(cryptogram_i_plus_one, body + (i + 1) * byte_len(monkey_params.block_size), Z, byte_len(body_i_plus_one_size));
		write_bits_to_index(output_cryptogram, cryptogram_i_plus_one, (i + 1) * monkey_params.block_size, body_i_plus_one_size);
	}

	memset(step_input, 0, step_input_bytes);
	extracted_block_size = extract_block(step_input, body, body_len, monkey_params.block_size, body_block_len - 1);
	set_bit_one(step_input, extracted_block_size);
	unsigned char temp_tag[byte_len(monkey_params.block_size + tag_len - 1)];
	monkeyduplex_stride(temp_tag, monkey_params.block_size,
						step_input, extracted_block_size + 2,
						state, monkey_params);
	unsigned int temp_tag_len = monkey_params.block_size;
	while (temp_tag_len < tag_len) {
		unsigned char zero_array[1] = {0};
		monkeyduplex_step(temp_tag + byte_len(temp_tag_len), monkey_params.block_size,
						  zero_array, 1,
						  state, monkey_params);
		temp_tag_len += monkey_params.block_size;
	}
	write_bits_to_index(output_tag, temp_tag, 0, tag_len);
}

// Caller must allocate memory for output
void pack_key(unsigned char* output, const unsigned char *key, int key_len, int keypack_len)
{
	memset(output, 0, byte_len(keypack_len));

	unsigned char keypack_len_bytes = (unsigned char)(keypack_len / 8);

	write_bits_to_index(output, &keypack_len_bytes, 0, 8);
	write_bits_to_index(output, key, 8, key_len);
	set_bit_one(output, 8 + key_len);
}

/* Returns the amount of blocks required to be allocated for a bit string of length bit_len, when size of one
 * block (in bits) equals block_size
 */
int block_len(int bit_len, int block_size)
{
	return (bit_len/block_size) + (bit_len % block_size ? 1:0);
}

// returns size of extracted block
unsigned int extract_block(unsigned char *output, const unsigned char *input, int input_len, int block_size, int block_index)
{
	// If empty input, do nothing
	if (input_len == 0) {
		return 0;
	}

	int total_blocks = block_len(input_len, block_size);

	// If not the last block, or if last block is a full sized block, copy a full sized block
	if ((block_index < (total_blocks - 1)) || (input_len % block_size == 0)) {
		cpynbits(output, 0, input, block_size * block_index, block_size);
		return block_size;
	}
	// If last block that has a size smaller than block_size
	else {
		int last_block_size = input_len % block_size;
		cpynbits(output, 0, input, block_size * block_index, last_block_size);
		return last_block_size;
	}

}

unsigned int get_block_size(unsigned int input_len, unsigned int block_size, unsigned int block_index)
{
	if (input_len == 0) {
		return 0;
	}

	int ret = block_size;
	unsigned int total_blocks = block_len(input_len, block_size);

	if ((block_index == (total_blocks -1)) && (input_len % block_size > 0)) {
		ret = input_len % block_size;
	}
	return ret;
}

void xor_n_bytes(unsigned char *output, const unsigned char *arr1, const unsigned char *arr2, unsigned int n)
{
	for (unsigned int i = 0; i < n; i++) {
		output[i] = arr1[i] ^ arr2[i];
	}
}



