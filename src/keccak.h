#ifndef KECCAK_H
#define KECCAK_H

/* Perform the KECCAK-p*[b, n_r] permutation
 *
 * S  - the input bit string
 * b  - the length of the input string in bits
 * nr - the number of rounds
 * l  - the value of l associated with b (log2(b/25))
 *
 * Returns a pointer to the output bit string
 */
unsigned char *keccak_p_star(unsigned char *S, unsigned long b, int nr, int l);

/* Utility functions */
void cpynbits(unsigned char *dst, unsigned int dst_o,
	      const unsigned char *src, unsigned int src_o, unsigned int n);
unsigned long concatenate(unsigned char **Z,
			  const unsigned char *X, unsigned long X_len,
			  const unsigned char *Y, unsigned long Y_len);
unsigned long concatenate_00(unsigned char **Z, const unsigned char *X,
			     unsigned long X_len);
unsigned long concatenate_01(unsigned char **Z, const unsigned char *X,
			     unsigned long X_len);
unsigned long concatenate_10(unsigned char **Z, const unsigned char *X,
			     unsigned long X_len);
unsigned long concatenate_11(unsigned char **Z, const unsigned char *X,
			     unsigned long X_len);
unsigned long pad10x1(unsigned char **P, unsigned int x, unsigned int m);

/* If needed, you can add your own functions below this line.
 * Do NOT modify anything above. */

int get_column_array_index(int x, int z);
int get_state_array_index(int x, int y, int z);

void set_bit_zero(unsigned char *array, int n);
void set_bit_one(unsigned char *array, int n);
void set_bit(unsigned char *array, int n, int value);
int get_bit(const unsigned char *array,  int n);
void get_bits_from_index(unsigned char *output, const unsigned char *input, int first_bit_index, int amount_of_bits);
void write_bits_to_index(unsigned char *output, const unsigned char *input, int first_written_bit, int amount_of_bits);

void Rnd(unsigned char *output, const unsigned char *input, int round);
void keccak_p_1600(unsigned char *output, const unsigned char *input, int rounds);
void keccak(const int c, unsigned char *output, int output_len, const unsigned char *input, int input_len);

void step_mapping_1_theta(unsigned char *output, const unsigned char *input);
void step_mapping_2_rho(unsigned char *output, const unsigned char *input);
void step_mapping_3_pi(unsigned char *output, const unsigned char *input);
void step_mapping_4_chi(unsigned char *output, const unsigned char *input);
void step_mapping_5_iota(unsigned char *output, const unsigned char *input, int round);
void step_mapping_3_pi_inverse(unsigned char *output, const unsigned char *input);

int mod(int a, int b);

int byte_len(int bit_len);

#endif				/* KECCAK_H */
